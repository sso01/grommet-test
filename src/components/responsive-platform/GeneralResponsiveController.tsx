import React from "react";
import Desktop from "./Desktop";
import Mobile from "./Mobile";

export interface IGeneralResponsiveControllerProps {
  readonly desktopComponent?: React.ReactNode;
  readonly mobileComponent?: React.ReactNode;
}

export class GeneralResponsiveController extends React.Component<IGeneralResponsiveControllerProps> {
  public render(): React.ReactNode {
    return (
      <>
        <Desktop>{this.props.desktopComponent}</Desktop>
        <Mobile>{this.props.mobileComponent}</Mobile>
      </>
    );
  }
}
