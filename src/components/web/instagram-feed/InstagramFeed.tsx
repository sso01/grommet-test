import React from "react";
import { Avatar, Box, Button, Grid, Image, Text } from "grommet";

export interface IInstagramFeedProps {
  readonly avatarUrl: string;
}
// https://scontent-dus1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/101700873_325030845159613_8527118999725198427_n.jpg?_nc_ht=scontent-dus1-1.cdninstagram.com&_nc_cat=101&_nc_ohc=QMqB0E_XkgYAX_zn5bN&tp=1&oh=519c1c679b8332dec52391d93232a163&oe=60118D23
export class InstagramFeed extends React.Component<IInstagramFeedProps> {
  public render(): React.ReactNode {
    return (
      <Box flex={"grow"} fill>
        <Box align={"center"}>
          <Text margin={"medium"} weight={"bold"} size={"xlarge"}>
            Follow us on instagram
          </Text>
          <Box align={"center"} pad={"large"} gap={"xsmall"}>
            <Avatar
              style={{ borderColor: "#1567d2" }}
              size={"xlarge"}
              src={this.props.avatarUrl}
              border={{ color: "red", size: "small" }}
            />
            <Text size={"small"} style={{ opacity: 0.75 }}>
              @alpha_eat_greek
            </Text>
          </Box>
        </Box>
        <Box direction={"row-responsive"} align={"center"} justify={"center"}>
          <Grid
            onTouchStart={() => console.log("yo")}
            columns={{
              count: 3,
              size: "small",
            }}
            gap="large"
          >
            <InstagramImageCard
              src={
                "https://scontent-muc2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/94197191_160606228755209_3270573708036999292_n.jpg?_nc_ht=scontent-muc2-1.cdninstagram.com&_nc_cat=105&_nc_ohc=1ju9lYDRBEIAX_6UiMi&tp=1&oh=69ab19d105e7bc88dd9dfde08c7556d7&oe=600FAC33"
              }
            />
            <InstagramImageCard
              src={
                "https://scontent-muc2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/103016476_158581705781326_702001392724237536_n.jpg?_nc_ht=scontent-muc2-1.cdninstagram.com&_nc_cat=110&_nc_ohc=hWqzDoE2XVAAX_OLL_I&tp=1&oh=e383145cf81175b15f41f07ebbd6d2f1&oe=6010EE67"
              }
            />
            <InstagramImageCard
              src={
                "https://scontent-muc2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/74338606_3058725067536453_6976336321414176090_n.jpg?_nc_ht=scontent-muc2-1.cdninstagram.com&_nc_cat=109&_nc_ohc=VWVOU7MPB98AX-MHwKa&tp=1&oh=82fc1d3ffb000c84baca4a856cca0044&oe=60117B6F"
              }
            />
            <InstagramImageCard
              src={
                "https://scontent-muc2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/117892317_3912407465452390_2147652267188842403_n.jpg?_nc_ht=scontent-muc2-1.cdninstagram.com&_nc_cat=110&_nc_ohc=zfBtnBXtCZ0AX8BXjza&tp=1&oh=706f37261b4d68930b9728003e9d1068&oe=600F75E0"
              }
            />
            <InstagramImageCard
              src={
                "https://scontent-muc2-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p750x750/122206176_1279826239020460_2963747063243928053_n.jpg?_nc_ht=scontent-muc2-1.cdninstagram.com&_nc_cat=104&_nc_ohc=B8uI8ZF0JJ0AX-t7hbV&tp=1&oh=409677c9d5374d28d6070b430acd162b&oe=6010F237"
              }
            />
          </Grid>
        </Box>
      </Box>
    );
  }
}

export const InstagramImageCard = ({ src }) => (
  <Button
    onClick={() =>
      window.open("https://www.instagram.com/alpha_eat_greek/", "_blank")
    }
  >
    <Image width={400} height={400} src={src} />
  </Button>
);
