import React from "react";
import {
  Avatar,
  Box,
  Carousel,
  Grid,
  Heading,
  ResponsiveContext,
  Text,
} from "grommet";
import { GoogleReviewService } from "../../../infrastructure/google-review/GoogleReviewService";
import { IGoogleReview } from "../../../infrastructure/google-review/IGoogleReviewService";
import Rating from "react-rating";

export interface IGoogleReviewProps {}

export class GoogleReviewState {
  public information: IGoogleReview;
}

export class GoogleReviewCard extends React.Component<
  IGoogleReviewProps,
  GoogleReviewState
> {
  public constructor(props: IGoogleReviewProps) {
    super(props);
    this.state = new GoogleReviewState();
  }

  public render(): React.ReactNode {
    return this.renderInformation();
  }

  public async componentDidMount(): Promise<void> {
    const information = await GoogleReviewService.getInformation(
      "ChIJf5IaSSovmEcRAwirLjBfQE0"
    );
    this.setState({ information });
  }

  private renderInformation(): React.ReactNode {
    if (this.state.information == undefined) {
      return <Box />;
    }
    return (
          <Grid fill columns={{ count: 4, size: "auto" }} justify={"center"}>
            {this.state.information.reviews.slice(0, 4).map((x) => (
              <Box direction={"row-responsive"} align={"center"} justify={"center"} >
                <Box gap={"small"} pad={"small"}>
                  <Box direction={"row"} align={"center"} gap={"small"}>
                    <Avatar src={x.profilePicture} pad={"medium"} />
                    <Box direction={"column"} responsive gap={"xsmall"}>
                      <Text weight={"bold"} size={"medium"}>
                        {x.authorName}
                      </Text>
                      <Box direction={"row-responsive"} gap={"small"}>
                      <Rating
                        fractions={2}
                        readonly
                        initialRating={x.rating}
                        emptySymbol={[
                          "fa fa-star-o fa-1x low",
                          "fa fa-star-o fa-1x low",
                          "fa fa-star-o fa-1x medium",
                          "fa fa-star-o fa-1x medium",
                          "fa fa-star-o fa-1x high",
                          "fa fa-star-o fa-1x high",
                        ]}
                        fullSymbol={[
                          "fa fa-star fa-1x low",
                          "fa fa-star fa-1x low",
                          "fa fa-star fa-1x medium",
                          "fa fa-star fa-1x medium",
                          "fa fa-star fa-1x high",
                          "fa fa-star fa-1x high",
                        ]}
                      />
                      <Text>{x.time}</Text>
                      </Box>
                    </Box>
                  </Box>
                  <Text margin={"xsmall"}>{x.description}</Text>
                </Box>
              </Box>
            ))}
          </Grid>
    );
  }
}
