import {
  Box,
  Button,
  Grid,
  Heading,
  Main,
  ResponsiveContext,
  Text,
} from "grommet";
import React from "react";
import DataUris from "../../../resources/data-uris.json";
import { ITopPageProps, TopPage } from "./TopPage";
import * as Scroll from "react-scroll";
import { IDish } from "../../../infrastructure/dish/IDish";
import { CheckApp } from "../general/CheckApp";
import { GoogleReview } from "../google-review/GoogleReview";
import { GoogleReviewCard } from "../google-review/GoogleReviewCard";
import { InstagramFeed } from "../instagram-feed/InstagramFeed";

export interface IDashboardProps {
  readonly topPageProps: ITopPageProps;
  readonly dishes: IDish[];
  readonly onViewMorePressed: () => void;
  readonly onGooglePlayClick: () => void;
  readonly onAppStoreClick: () => void;
  readonly onDishClick: (dishId: string) => void;
}

class Dashboard extends React.Component<IDashboardProps> {
  public render(): React.ReactNode {
    return (
      <Main>
        <Scroll.Element name={"top-section"} id={"top-section"}>
          <Box
            basis={"large"}
            background={{
              image: `url(${DataUris.background})`,
              repeat: "no-repeat",
            }}
          >
            <TopPage
              {...this.props.topPageProps}
              onGoButtonClicked={() =>
                Scroll.scroller.scrollTo("google-review-section", {
                  smooth: true,
                })
              }
            />
            <Box basis={"large"} />
          </Box>
          <Box basis={"small"} />
        </Scroll.Element>
        <Scroll.Element name={"dish-section"} id={"dish-section"}>
          <Box>
            <Box
              direction={"row"}
              align={"center"}
              justify={"between"}
              pad={"medium"}
            >
              <Heading margin={{ left: "medium" }} size={"small"}>
                Tried already?
              </Heading>
              <Button plain onClick={() => this.props.onViewMorePressed()}>
                View all
              </Button>
            </Box>
            {this.renderDishList()}
          </Box>
        </Scroll.Element>
        <Scroll.Element name={"google-review-section"}>
          <CheckApp
            onGooglePlayClick={() => this.props.onGooglePlayClick()}
            onAppStoreClick={() => this.props.onAppStoreClick()}
          />
          <GoogleReview />

          <Box pad={"xlarge"}>
            <GoogleReviewCard />
          </Box>
        </Scroll.Element>
        <Box pad={"xlarge"}>
          <InstagramFeed
            avatarUrl={
              "https://scontent-dus1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/101700873_325030845159613_8527118999725198427_n.jpg?_nc_ht=scontent-dus1-1.cdninstagram.com&_nc_cat=101&_nc_ohc=QMqB0E_XkgYAX_zn5bN&tp=1&oh=519c1c679b8332dec52391d93232a163&oe=60118D23"
            }
          />
        </Box>
      </Main>
    );
  }

  private renderDishList(): React.ReactNode {
    return (
      <ResponsiveContext.Consumer>
        {(value) => (
          <Grid
            columns={{ count: value === "large" ? 8 : 4, size: "auto" }}
            responsive
            fill
            pad={"large"}
            gap={"large"}
          >
            {this.props.dishes.map((x) => (
              <Box fill gap={"small"} align={"center"} justify={"center"}>
                <Box
                  onClick={() => this.props.onDishClick(x.id)}
                  round={"xlarge"}
                  width={"small"}
                  height={"small"}
                  background={{
                    size: "cover",
                    position: "center",
                    image: `url(${x.image})`,
                    repeat: "no-repeat",
                  }}
                  basis={"small"}
                />
                <Text weight={"bold"} size={"medium"}>
                  {x.name}
                </Text>
              </Box>
            ))}
          </Grid>
        )}
      </ResponsiveContext.Consumer>
    );
  }
}

export default Dashboard;
