import React from "react";
import { WebGeneralHeader } from "../general/WebGeneralHeader";
import { Box, Heading } from "grommet";
import { CustomPlacesAutocomplete } from "../../cross-platform/general/CustomPlacesAutocomplete";
import { Clock, Location } from "grommet-icons";
import { CustomInput } from "../../cross-platform/general/CustomInput";
import { FormatFactory } from "../../../infrastructure/format-factory/FormatFactory";

export interface ITopPageProps {
  readonly address: string;
  readonly deliveryHour: string;
  readonly onAddressChange: (address: string) => void;
  readonly onDeliveryHourChange: (deliveryHour: string) => void;
  readonly onGoButtonClicked: () => void;
}

export class TopPage extends React.Component<ITopPageProps> {
  public render(): React.ReactNode {
    return (
      <React.Fragment>
        <WebGeneralHeader />
        <Box
          alignSelf={"start"}
          pad={"xlarge"}
          gap={"medium"}
        >
          <Box
            background={{ color: "rgba(62,104,127,0.70)" }}
            round={{ size: "xsmall" }}
          >
            <Heading margin={"medium"}>
              Hungry ?, you're in the right place
            </Heading>
          </Box>
          <Box direction={"row-responsive"} gap={"large"} justify={"start"}>
            <CustomPlacesAutocomplete
              containerProps={{
                background: { color: "white" },
                round: "xsmall",
                width: "medium",
              }}
              inputProps={{ placeholder: "Delivery Address" }}
              searchOptions={{ types: ["address"] }}
              address={this.props.address}
              onChange={(address) => this.props.onAddressChange(address)}
              icon={<Location />}
            />
            <Box basis={"small"}>
              <CustomInput
                format={FormatFactory.getDeliveryDateFormat()}
                value={this.props.deliveryHour}
                inputProps={{ icon: <Clock /> }}
                onChange={(deliveryHour: string) =>
                  this.props.onDeliveryHourChange(deliveryHour)
                }
              />
            </Box>
            <Box
              justify={"center"}
              align={"center"}
              round={"xxsmall"}
              basis={"xsmall"}
              height={"xxsmall"}
              background={{ color: "rgba(62,104,127,0.70)" }}
              onClick={() => this.props.onGoButtonClicked()}
            >
              <Heading size={"small"}>GO</Heading>
            </Box>
          </Box>
        </Box>
      </React.Fragment>
    );
  }
}
