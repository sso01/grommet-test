import React from "react";
import { Box, Heading, Main, Text } from "grommet";
import { AppleAppStore, GooglePlay } from "grommet-icons";

export interface ICheckApp {
  readonly onGooglePlayClick: () => void;
  readonly onAppStoreClick: () => void;
}

export class CheckApp extends React.Component<ICheckApp> {
  public render(): React.ReactNode {
    return (
      <Main direction={"row"} justify={"center"} align={"center"} gap={"large"}>
        <Heading fill size={"medium"} margin={"large"}>
          There's more to love in the app.
        </Heading>
        <Box direction={"row"} gap={"large"} justify={"end"} align={"center"}>
          <Box
            direction={"row"}
            align={"center"}
            basis={"small"}
            round={"small"}
            flex={"grow"}
            background={{ color: "#e2e2e2" }}
            pad={"small"}
            gap={"small"}
            onClick={() => this.props.onAppStoreClick()}
          >
            <AppleAppStore size={"medium"} />
            <Text weight={"bold"}>Apple Store</Text>
          </Box>
          <Box
            direction={"row"}
            align={"center"}
            basis={"small"}
            round={"small"}
            flex={"grow"}
            background={{ color: "#e2e2e2" }}
            pad={"small"}
            gap={"small"}
            onClick={() => this.props.onGooglePlayClick()}
          >
            <GooglePlay size={"medium"} />
            <Text weight={"bold"}>Google Play</Text>
          </Box>
        </Box>
      </Main>
    );
  }
}
