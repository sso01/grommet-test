import * as React from "react";
import withInstagramFeed from "origen-react-instagram-feed";
import compose from "recompose/compose";
import { Box, Button, Carousel, Grid, Image } from "grommet";

const styles = () => ({
  wrapper: {},
  image: {
    width: "100%",
    height: "100%",
  },
});

export type Props = {
  media?: Array<{
    displayImage: string;
    id?: string;
    postLink?: string;
    accessibilityCaption?: string;
  }>;
  account: string;
  status: "completed" | "loading" | "failed";
};

const InstaGrid = ({ media, account, status }: Props) => {
  console.log(media, "media");
  return (
    <Carousel fill>
      {media &&
        status === "completed" &&
        media.map((x, i) => (
          <Box>
            <Box
              basis={"medium"}
              direction={"row"}
              background={{ image: `url(${x.displayImage})` }}
            />
          </Box>
        ))}
      {status === "loading" && <p>loading...</p>}
      {status === "failed" && <p>Check instagram here</p>}
    </Carousel>
  );
};

export default withInstagramFeed(InstaGrid);
