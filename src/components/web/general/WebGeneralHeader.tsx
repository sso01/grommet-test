import React from "react";
import { Box, Button, Header, Image, Text } from "grommet";
import { Language, Menu as MenuIcon } from "grommet-icons";
import DataUris from "../../../resources/data-uris.json";

export class WebGeneralHeader extends React.Component {
  public render(): React.ReactNode {
    return (
      <Header
        fill
        background={{ color: "rgba(62,104,127,0.70)" }}
        justify={"between"}
        basis={"xsmall"}
      >
        <Box basis={"xsmall"} hoverIndicator={false}>
          <Button
            focusIndicator={false}
            icon={<MenuIcon size={"large"} />}
            hoverIndicator
          />
        </Box>
        <Box
          direction={"row-responsive"}
          align={"center"}
          justify={"center"}
          basis={"xxsmall"}
        >
          <Text size={"xlarge"} weight={"bold"}>
            Alpha
          </Text>
          <Image src={DataUris.logo} margin={"small"} height={80} />
          <Text size={"xlarge"} weight={"bold"}>
            Greek
          </Text>
        </Box>
        <Box direction={"row-responsive"} basis={"small"}>
          <Box justify={"center"} align={"center"}>
            <Button
              focusIndicator={false}
              icon={<Language size={"medium"} />}
            />
          </Box>
          <Box pad="small">
            <Button label="Sing In" onClick={() => {}} />
          </Box>
        </Box>
      </Header>
    );
  }
}
