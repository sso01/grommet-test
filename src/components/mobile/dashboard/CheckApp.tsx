import React from "react";
import { Box, Heading, Main, Text } from "grommet";
import { AppleAppStore, GooglePlay } from "grommet-icons";

export interface ICheckApp {
  readonly onGooglePlayClick: () => void;
  readonly onAppStoreClick: () => void;
}

export class CheckApp extends React.Component<ICheckApp> {
  public render(): React.ReactNode {
    return (
      <Main align={"center"}>
        <Heading fill size={"medium"}>
          There's more to love in the app.
        </Heading>
        <Box
          direction={"row"}
          basis={"xsmall"}
          fill
          gap={"xlarge"}
          pad={"large"}
          justify={"center"}
          align={"center"}
        >
          <Box
            align={"center"}
            justify={"center"}
            gap={"small"}
            basis={"small"}
            round={"large"}
            pad={"small"}
            onClick={() => this.props.onAppStoreClick()}
            background={{ color: "#e2e2e2" }}
          >
            <AppleAppStore size={"medium"} />
            <Text weight={"bold"}>App Store</Text>
          </Box>
          <Box
            align={"center"}
            justify={"center"}
            gap={"small"}
            basis={"small"}
            round={"large"}
            pad={"small"}
            onClick={() => this.props.onGooglePlayClick()}
            background={{ color: "#e2e2e2" }}
          >
            <GooglePlay size={"medium"} />
            <Text weight={"bold"}>Google Play</Text>
          </Box>
        </Box>
      </Main>
    );
  }
}
