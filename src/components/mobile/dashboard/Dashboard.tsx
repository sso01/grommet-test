import React from "react";
import { Box, Button, Card, Grid, Heading, Main, Text } from "grommet";
import DataUris from "../../../resources/data-uris.json";
import { TopPage } from "./TopPage";
import * as Scroll from "react-scroll";
import { IDish } from "../../../infrastructure/dish/IDish";
import { MobileGoogleReviewCard } from "../google-review/MobileGoogleReviewCard";
import { MobileGoogleReview } from "../google-review/MobileGoogleReview";
import { MobileInstagramFeed } from "../instagram-feed/MobileInstagramFeed";

export interface IDashboardProps {
  readonly address: string;
  readonly deliveryHour: string;
  readonly onAddressChange: (address: string) => void;
  readonly onDeliveryHourChange: (deliveryHour: string) => void;
  readonly dishes: IDish[];
  readonly onViewMorePressed: () => void;
  readonly onGooglePlayClick: () => void;
  readonly onAppStoreClick: () => void;
  readonly onDishClick: (dishId: string) => void;
}

class Dashboard extends React.Component<IDashboardProps> {
  public render(): React.ReactNode {
    return (
      <Main>
        <Scroll.Element name={"top-section"} id={"top-section"}>
          <Box
            background={{
              image: `url(${DataUris.background})`,
              repeat: "no-repeat",
            }}
          >
            <TopPage
              {...this.props}
              onGoButtonClicked={() =>
                Scroll.scroller.scrollTo("dish-section", { smooth: true })
              }
            />
            <Box fill basis={"xsmall"} />
          </Box>
        </Scroll.Element>
        <Scroll.Element name={"dish-section"} id={"dish-section"}>
          <Box>
            <Box
              direction={"row"}
              align={"center"}
              justify={"between"}
              pad={"medium"}
            >
              <Heading margin={{ left: "medium" }} size={"small"}>
                Tried already?
              </Heading>
              <Button plain onClick={() => this.props.onViewMorePressed()}>
                View all
              </Button>
            </Box>
            {this.renderDishList()}
          </Box>
        </Scroll.Element>
        <Scroll.Element name={"google-review-section"}>
          <MobileGoogleReview />
          <Card pad={"large"} margin={"medium"}>
            <MobileGoogleReviewCard />
          </Card>
        </Scroll.Element>
        <Box pad={"xlarge"}>
          <MobileInstagramFeed
            avatarUrl={
              "https://scontent-dus1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/p640x640/101700873_325030845159613_8527118999725198427_n.jpg?_nc_ht=scontent-dus1-1.cdninstagram.com&_nc_cat=101&_nc_ohc=QMqB0E_XkgYAX_zn5bN&tp=1&oh=519c1c679b8332dec52391d93232a163&oe=60118D23"
            }
          />
        </Box>
      </Main>
    );
  }

  private renderDishList(): React.ReactNode {
    return (
      <Grid columns={{ count: 3, size: "auto" }}>
        {this.props.dishes.slice(0, 3).map((x) => (
          <Box fill gap={"medium"} align={"center"} justify={"center"}>
            <Box
              onClick={() => this.props.onDishClick(x.id)}
              round={"xlarge"}
              width={"xsmall"}
              height={"xxsmall"}
              background={{
                size: "cover",
                position: "center",
                image: `url(${x.image})`,
                repeat: "no-repeat",
              }}
              basis={"xsmall"}
            />
            <Text weight={"bold"} size={"medium"}>
              {x.name}
            </Text>
          </Box>
        ))}
      </Grid>
    );
  }
}

export default Dashboard;
