import React from "react";
import { Box, Main, Markdown, Text } from "grommet";
import { CustomPlacesAutocomplete } from "../../cross-platform/general/CustomPlacesAutocomplete";
import { Clock, Location } from "grommet-icons";
import { CustomInput } from "../../cross-platform/general/CustomInput";
import { FormatFactory } from "../../../infrastructure/format-factory/FormatFactory";

export interface IDashboardProps {
  readonly address: string;
  readonly deliveryHour: string;
  readonly onAddressChange: (address: string) => void;
  readonly onDeliveryHourChange: (deliveryHour: string) => void;
  readonly onGoButtonClicked: () => void;
}

export class TopPage extends React.Component<IDashboardProps> {
  public render(): React.ReactNode {
    return (
      <Box fill basis={"large"} justify={"center"}>
        <Box pad={"xlarge"}  round={{ size: "small" }}>
          <Text color={"#ffffff"} weight={"bold"} size={"xlarge"}>
            Hungry ?, you're in the right place
          </Text>
        </Box>
        <Box pad={"small"} gap={"xlarge"}>
          <CustomPlacesAutocomplete
            containerProps={{
              background: { color: "white" },
              round: "xsmall",
            }}
            inputProps={{ placeholder: "Delivery Address" }}
            searchOptions={{ types: ["address"] }}
            address={this.props.address}
            onChange={(address) => this.props.onAddressChange(address)}
            icon={<Location />}
          />
          <Box direction={"row"} gap={"medium"}>
            <CustomInput
              containerProps={{ width: "small" }}
              format={FormatFactory.getDeliveryDateFormat()}
              value={this.props.deliveryHour}
              inputProps={{ icon: <Clock />, size: "small" }}
              onChange={(deliveryHour: string) =>
                this.props.onDeliveryHourChange(deliveryHour)
              }
            />
            <Box
              background={{ size: "small", color: "rgb(63,95,114)" }}
              align={"center"}
              justify={"center"}
              width={"small"}
              onClick={() => this.props.onGoButtonClicked()}
            >
              <Text weight={"bold"}>GO !</Text>
            </Box>
          </Box>
          <Box
            align={"start"}
            justify={"start"}
            width={"medium"}
            background={{ color: "rgb(63,95,114, 0.5)" }}
            round={"xsmall"}
            pad={"small"}
          >
            <Markdown>Don't have an account ? [SIGN-UP]() **now**</Markdown>
          </Box>
        </Box>
      </Box>
    );
  }
}
