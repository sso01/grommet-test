import React from "react";
import { Box, Button, Header, Heading, Image, Text } from "grommet";
import { Language, Menu as MenuIcon } from "grommet-icons";
import DataUris from "../../../resources/data-uris.json";

export class MobileGeneralHeader extends React.Component {
  public render(): React.ReactNode {
    return (
      <Header
        fill
        style={{ opacity: 0.87 }}
        justify={"between"}
        basis={"xsmall"}
      >
        <Image src={DataUris.logo} height={70} />
        <Heading size={"medium"} color={"rgb(245,245,245)"}>Alpha Eat Greek</Heading>
        <Box direction={"row-responsive"} basis={"small"}>
          <Box justify={"center"} align={"center"}>
            <Button
              focusIndicator={false}
              icon={<MenuIcon size={"large"} />}
              hoverIndicator
            />
          </Box>
        </Box>
      </Header>
    );
  }
}
