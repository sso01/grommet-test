import React from "react";
import { Avatar, Box, Card, Carousel, Grid, Heading, Text } from "grommet";
import { GoogleReviewService } from "../../../infrastructure/google-review/GoogleReviewService";
import {
  IGoogleReview,
  IReview,
} from "../../../infrastructure/google-review/IGoogleReviewService";
import Rating from "react-rating";

export interface IGoogleReviewProps {}

export class GoogleReviewState {
  public information: IGoogleReview;
}

export class MobileGoogleReviewCard extends React.Component<
  IGoogleReviewProps,
  GoogleReviewState
> {
  public constructor(props: IGoogleReviewProps) {
    super(props);
    this.state = new GoogleReviewState();
  }

  public render(): React.ReactNode {
    return this.renderInformation();
  }

  public async componentDidMount(): Promise<void> {
    const information = await GoogleReviewService.getInformation(
      "ChIJf5IaSSovmEcRAwirLjBfQE0"
    );
    this.setState({ information });
  }

  private renderInformation(): React.ReactNode {
    if (this.state.information == undefined) {
      return <Box />;
    }
    return (
      <Carousel controls={false} play={3000} >
        {this.getFilteredResults(this.state.information)
          .slice(0, 3)
          .map((x, i) => this.renderNormalCard(x))}
      </Carousel>
    );
  }

  private getFilteredResults(information: IGoogleReview): IReview[] {
    return information.reviews
      .filter((x) => x.description.length > 20 )
      .slice(0, 3);
  }

  private renderNormalCard(review: IReview): React.ReactNode {
    return (
      <Box basis={"small"}>
        <Box direction={"row"} align={"center"} gap={"small"}>
          <Avatar src={review.profilePicture} pad={"medium"} />
          <Box>
            <Text weight={"bold"} size={"medium"}>
              {review.authorName}
            </Text>
            <Box direction={"row"} gap={"small"}>
              <Rating
                fractions={3}
                readonly
                initialRating={review.rating}
                emptySymbol={[
                  "fa fa-star-o fa-1x low",
                  "fa fa-star-o fa-1x low",
                  "fa fa-star-o fa-1x medium",
                  "fa fa-star-o fa-1x medium",
                  "fa fa-star-o fa-1x high",
                  "fa fa-star-o fa-1x high",
                ]}
                fullSymbol={[
                  "fa fa-star fa-1x low",
                  "fa fa-star fa-1x low",
                  "fa fa-star fa-1x medium",
                  "fa fa-star fa-1x medium",
                  "fa fa-star fa-1x high",
                  "fa fa-star fa-1x high",
                ]}
              />
              <Text>{review.time}</Text>
            </Box>
          </Box>
        </Box>
        <Text margin={"xsmall"}>
          {this.getTruncateReview(review.description)}
        </Text>
      </Box>
    );
  }

  private getTruncateReview(description: string): string {
    const words = description.split(" ");
    console.log(words.length, "length")
    if (words.length > 20) {
      return `${words.slice(0, 20).join(" ")}. ${words[21].charAt(0).toUpperCase()} ...`;
    } else {
      return description;
    }
  }
}
