import React from "react";
import { Box, Button, Card, Heading, Text } from "grommet";
import { GoogleReviewService } from "../../../infrastructure/google-review/GoogleReviewService";
import { IGoogleReview } from "../../../infrastructure/google-review/IGoogleReviewService";
import Rating from "react-rating";

export interface IGoogleReviewProps {}

export class GoogleReviewState {
  public information: IGoogleReview;
}

export class MobileGoogleReview extends React.Component<
  IGoogleReviewProps,
  GoogleReviewState
> {
  public constructor(props: IGoogleReviewProps) {
    super(props);
    this.state = new GoogleReviewState();
  }
  public render(): React.ReactNode {
    return (
      <Box pad={{top: "xlarge"}}>
        <Box
          direction={"column"}
          align={"center"}
          justify={"center"}
          pad={"large"}
          gap={"large"}
          background={{ color: "#74909f" }}
        >
          <Text color={"white"} weight={"bold"} size={"large"}>
            Would you rate us on Google?
          </Text>
          <Box gap={"medium"} direction={"row"}>
            <Button
              onClick={() =>
                window.open(
                  "https://www.google.com/maps/place/Alpha+Eat+Greek/@49.1411285,9.2193018,17z/data=!4m5!3m4!1s0x47982f2a491a927f:0x4d405f302eab0803!8m2!3d49.14116!4d9.21917",
                  "_blank"
                )
              }
              style={{
                borderRadius: 5,
                background: "#4775d2",
                borderColor: "#4775d2",
                color: "white",
              }}
              label={"YES"}
            />
            <Button
              style={{
                borderRadius: 5,
                background: "white",
                borderColor: "white",
              }}
              label={"NO"}
            />
          </Box>
        </Box>
      </Box>
    );
  }

  public async componentDidMount(): Promise<void> {
    const information = await GoogleReviewService.getInformation(
      "ChIJf5IaSSovmEcRAwirLjBfQE0"
    );
    this.setState({ information });
  }

  private renderInformation(): React.ReactNode {
    if (this.state.information == undefined) {
      return <Box />;
    }
    return (
      <Card
        direction={"row"}
        align={"center"}
      >
          <Box direction={"column"} fill flex={"grow"}>
              <Text weight={"bold"} size={"small"}>
                  {this.state.information.name}
              </Text>
              <Box direction={"row"} gap={"medium"}>
                  <Text weight={"bold"} size={"small"}>
                      {this.state.information.rating}
                  </Text>
                  <Box basis={"xsmall"}>
                      <Rating
                          fractions={2}
                          readonly
                          initialRating={this.state.information.rating}
                          emptySymbol={[
                              "fa fa-star-o fa-1x low",
                              "fa fa-star-o fa-1x low",
                              "fa fa-star-o fa-1x medium",
                              "fa fa-star-o fa-1x medium",
                              "fa fa-star-o fa-1x high",
                              "fa fa-star-o fa-1x high",
                          ]}
                          fullSymbol={[
                              "fa fa-star fa-1x low",
                              "fa fa-star fa-1x low",
                              "fa fa-star fa-1x medium",
                              "fa fa-star fa-1x medium",
                              "fa fa-star fa-1x high",
                              "fa fa-star fa-1x high",
                          ]}
                      />
                  </Box>
              </Box>
          </Box>
        </Card>
    );
  }
}
