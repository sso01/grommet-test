import React from "react";
import {Box, MaskedInput} from "grommet";
import { MaskedInputProps } from "grommet/components/MaskedInput";
import {BoxProps} from "grommet/components/Box";

export type CustomInputFormat = Array<{
  length?: number | number[];
  fixed?: string;
  options?: string[] | number[];
  regexp?: {};
  placeholder?: string;
}>;

export interface ICustomInputProps {
  readonly value: string;
  readonly onChange: (value: string) => void;
  readonly format: CustomInputFormat;
  readonly placeholder?: string;
  readonly inputProps?: MaskedInputProps;
  readonly containerProps?: BoxProps;
}

export class CustomInput extends React.Component<ICustomInputProps> {
  public render(): React.ReactNode {
    return (
      <Box {...this.props.containerProps}>
        <MaskedInput
          style={{ backgroundColor: "white" }}
          value={this.props.value}
          onChange={(event) => this.props.onChange(event.target.value)}
          placeholder={this.props.placeholder}
          mask={this.props.format}
          {...this.props.inputProps}
        />
      </Box>
    );
  }
}
