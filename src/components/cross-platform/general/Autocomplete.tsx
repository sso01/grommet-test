import { Box, TextInput } from "grommet";
import React from "react";
import { TextInputProps } from "grommet/components/TextInput";
import { BoxProps } from "grommet/components/Box";

export interface IAutocompleteOption {
  readonly key: string;
  readonly value: string;
}

export interface IAutocompleteProps {
  readonly inputProps: TextInputProps;
  readonly options: IAutocompleteOption[];
  readonly containerProps?: BoxProps;
}

export class Autocomplete extends React.Component<IAutocompleteProps> {
  public render(): React.ReactNode {
    return (
      <Box>
        <Box {...this.props.containerProps}>
          <TextInput
            {...this.props.inputProps}
            suggestions={this.props.options}
          />
        </Box>
      </Box>
    );
  }
}
