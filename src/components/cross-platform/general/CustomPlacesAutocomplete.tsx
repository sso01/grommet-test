import React from "react";
import { Autocomplete, IAutocompleteOption } from "./Autocomplete";
import PlacesAutocomplete, {
  PropTypes,
  Suggestion,
} from "react-places-autocomplete";
import { TextInputProps } from "grommet/components/TextInput";
import { BoxProps } from "grommet/components/Box";

export interface ICustomAutocompleteProps {
  readonly address: string;
  readonly onChange: (value: string) => void;
  readonly onSelect?: (address: string, placeID: string) => void;
  readonly icon?: JSX.Element;
  readonly inputProps?: TextInputProps;
  readonly containerProps?: BoxProps;
  readonly searchOptions?: PropTypes["searchOptions"];
}

export class CustomPlacesAutocomplete extends React.Component<ICustomAutocompleteProps> {
  public render(): React.ReactNode {
    return (
      <PlacesAutocomplete
        value={this.props.address}
        onChange={(value) => this.props.onChange(value)}
        onSelect={(address, placeID) => this.handleSelected(address, placeID)}
        searchOptions={this.props.searchOptions}
      >
        {(params) => {
          return (
            <Autocomplete
              containerProps={this.props.containerProps}
              options={CustomPlacesAutocomplete.getOptions(params.suggestions)}
              inputProps={this.getInputProps(params)}
            />
          );
        }}
      </PlacesAutocomplete>
    );
  }

  private static getOptions(
    suggestions: ReadonlyArray<Suggestion>
  ): IAutocompleteOption[] {
    return suggestions.map((x) => ({ value: x.description, key: x.placeId }));
  }

  private handleSelected(address: string, placeID: string): void {
    const action = this.props.onSelect;
    if (action != undefined) {
      action(address, placeID);
    }
  }

  private getInputProps(params): TextInputProps {
    return {
      ...params.getInputProps(),
      ...this.props.inputProps,
      icon: this.props.icon,
    };
  }
}
