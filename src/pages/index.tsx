import React from "react";
import { IAutocompleteOption } from "../components/cross-platform/general/Autocomplete";
import { GeneralResponsiveController } from "../components/responsive-platform/GeneralResponsiveController";
import WebDashboard from "../components/web/dashboard/Dashboard";
import MobileDashboard from "../components/mobile/dashboard/Dashboard";
import { withRouter } from "next/router";
import { WithRouterProps } from "next/dist/client/with-router";
import { IDish } from "../infrastructure/graphql-types";
import {
  ApolloRequestType,
  ApolloService,
} from "../infrastructure/apollo-service";

export class HomeState {
  public selectedValue: string = "";
  public address: string = "";
  public deliveryHour: string = "";
  public suggestions: IAutocompleteOption[] = [];
  public dishes: IDish[] = [];
}

class Home extends React.Component<WithRouterProps, HomeState> {
  public constructor(props: WithRouterProps) {
    super(props);
    this.state = new HomeState();
  }

  public render(): React.ReactNode {
    return (
      <GeneralResponsiveController
        mobileComponent={
          <MobileDashboard
            dishes={this.state.dishes}
            deliveryHour={this.state.deliveryHour}
            address={this.state.address}
            onAddressChange={(address) => this.setState({ address })}
            onDeliveryHourChange={(deliveryHour) =>
              this.setState({ deliveryHour })
            }
            onGooglePlayClick={() => console.log("google play click")}
            onAppStoreClick={() => console.log("app store clicked")}
            onDishClick={(dishId: string) =>
              this.props.router.push(`dish/${dishId}`)
            }
            onViewMorePressed={() => console.log("View More")}
          />
        }
        desktopComponent={
          <WebDashboard
            dishes={this.state.dishes}
            onGooglePlayClick={() => console.log("google play click")}
            onAppStoreClick={() => console.log("app store clicked")}
            onDishClick={(dishId: string) =>
              this.props.router.push(`dish/${dishId}`)
            }
            onViewMorePressed={() => console.log("view more")}
            topPageProps={{
              address: this.state.address,
              deliveryHour: this.state.deliveryHour,
              onAddressChange: (address) => this.setState({ address }),
              onDeliveryHourChange: (deliveryHour) =>
                this.setState({ deliveryHour }),
              onGoButtonClicked: () => undefined,
            }}
          />
        }
      />
    );
  }

  public async componentDidMount(): Promise<void> {
    const dishes = await ApolloService.send<IDish, IDish[]>(
      ApolloRequestType.query,
      "DishList",
      {
        name: undefined,
        image: undefined,
        categoryId: undefined,
        price: undefined,
        id: undefined,
      }
    );
    this.setState({ dishes });
  }
}

export default withRouter(Home);
