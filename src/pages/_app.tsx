import App from "next/app";
import { Grommet, grommet as grommetTheme } from "grommet";
import React from "react";

export default class MyApp extends App {
  public render(): JSX.Element {
    const { Component, pageProps } = this.props;
    return (
      <Grommet
        theme={grommetTheme}
        style={{
          flex: 1,
          display: "flex",
          margin: -8,
        }}
      >
        <Component {...pageProps} />
      </Grommet>
    );
  }
}
