import React from "react";
import { Box, Card, Carousel, Heading, Image, Text, TextInput } from "grommet";
import { FormUp, FormDown } from "grommet-icons";
import { withRouter } from "next/router";
import { WithRouterProps } from "next/dist/client/with-router";
import {
  ApolloRequestType,
  ApolloService,
} from "../../infrastructure/apollo-service";
import { IDish } from "../../infrastructure/graphql-types";
import { BeatLoader } from "react-spinners";
import InputNumber from "rc-input-number";

export interface IDishProps extends WithRouterProps {
  readonly dish: IDish;
}

export class DishState {
  public dishAmount: number = 1;
}

export class Dish extends React.Component<IDishProps, DishState> {
  public constructor(props: IDishProps) {
    super(props);
    this.state = new DishState();
  }

  public render(): React.ReactNode {
    if (this.props.dish == undefined) {
      return <BeatLoader />;
    }
    return (
      <Box direction={"row"}>
        <Box pad={"large"} align={"start"} justify={"center"}>
          <Carousel>{this.renderImages()}</Carousel>
        </Box>
        <Card basis={"large"} margin={"large"}>
          <Heading alignSelf={"center"}>{this.props.dish.name}</Heading>
          <Text alignSelf={"center"} margin={"medium"} size={"small"}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id
            leo metus. Nunc sollicitudin suscipit viverra. In dignissim non enim
            quis ornare. Phasellus sapien magna, tincidunt sed imperdiet in,
            vehicula quis orci. Nulla ac ligula nec risus auctor vulputate.
            Integer nec nulla at quam semper malesuada sit amet a dui. In eget
            eros et nisi convallis pellentesque non sed ex. Sed tincidunt, nibh
            nec egestas congue, ligula elit hendrerit nisl, nec suscipit lorem
            ipsum auctor ante. Donec vel vehicula dolor. Vivamus egestas justo
            eu purus iaculis bibendum. Ut bibendum tortor at pharetra ornare.
            Donec tincidunt quam eu justo pulvinar dictum.
          </Text>
          <Box align={"start"} pad={"large"} basis={"xsmall"} direction={"row"} width={"small"} gap={"small"}>
            <TextInput
              size={"small"}
              defaultValue={1}
              value={this.state.dishAmount}
            />
            <Box>
              <FormUp
                onClick={() =>
                  this.setState({ dishAmount: this.state.dishAmount + 1 })
                }
              />
              <FormDown
                onClick={() =>
                  this.setState({ dishAmount: this.state.dishAmount - 1 })
                }
              />
            </Box>
          </Box>
        </Card>
      </Box>
    );
  }

  static async getInitialProps(ctx) {
    const dishId = ctx.query.id;
    const dish = await ApolloService.send<IDish>(
      ApolloRequestType.query,
      "Dish",
      { name: undefined, id: undefined, image: undefined, price: undefined },
      { id: dishId }
    );
    return { dish: dish, dishId: dishId };
  }

  private renderImages(): React.ReactNode {
    return [this.props.dish.image].map((x) => (
      <Image width={600} height={600} src={x} />
    ));
  }
}

export default withRouter(Dish);
