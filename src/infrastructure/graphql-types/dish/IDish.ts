export interface IDish {
  readonly id: string;
  readonly name: string;
  readonly price: number;
  readonly image: string;
  readonly categoryId: string;
}
