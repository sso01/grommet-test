import { ILocation } from "./ILocation";

export interface IOrderLocation {
  readonly id: string;
  readonly currentLocation: ILocation;
  readonly destinationLocation: ILocation;
  readonly trackingId: string;
}
