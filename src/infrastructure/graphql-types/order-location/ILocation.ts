export interface ILocation {
  readonly latitude: number;
  readonly longitude: number;
}
