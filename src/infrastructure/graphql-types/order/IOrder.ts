import {IDish} from "../dish";
import {IOrderLocation} from "../order-location";

export interface IOrder {
  readonly id: string;
  readonly name: string;
  readonly totalPrice: number;
  readonly orderLocationInformation?: IOrderLocation;
  readonly isDelivered: boolean;
  readonly isReady: boolean;
  readonly items: IDish[];
}
