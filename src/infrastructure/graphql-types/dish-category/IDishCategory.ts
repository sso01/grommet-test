export interface IDishCategory {
  readonly id: string;
  readonly category: string;
}
