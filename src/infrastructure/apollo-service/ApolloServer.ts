import { ApolloClient, gql, InMemoryCache } from "@apollo/client";
import { ApolloRequestType } from "./ApolloRequestType";
import { ApolloRequestFactory } from "./ApolloRequestFactory";

const client = new ApolloClient({
  uri: "http://localhost:3000",
  cache: new InMemoryCache(),
});

export class ApolloService {
  public static async send<TEntity, TReturnType = TEntity>(
    requestType: ApolloRequestType,
    entityName: string,
    expectedBody: Partial<TEntity>,
    query?: Partial<TEntity>
  ): Promise<TReturnType> {
    const apolloQuery = ApolloRequestFactory.create(
      requestType,
      entityName,
      expectedBody,
      query
    );
    switch (requestType) {
      case ApolloRequestType.mutation: {
        const { data } = await client.mutate({
          mutation: gql`
            ${apolloQuery}
          `,
        });
        return data[entityName];
      }
      case ApolloRequestType.query: {
        const { data } = await client.query({
          query: gql`${apolloQuery}`,
        });
        return data[entityName];
      }
    }
  }
}
