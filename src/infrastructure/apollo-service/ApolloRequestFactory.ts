import { ApolloRequestType } from "./ApolloRequestType";

export class ApolloRequestFactory {
  public static create<TEntity>(
    requestType: ApolloRequestType,
    entityName: string,
    expectedBody: Partial<TEntity>,
    query?: Partial<TEntity>
  ): string {
    return `${requestType} ${this.createQuery<TEntity>(
      entityName,
      expectedBody,
      query
    )}`;
  }

  private static createQuery<TEntity>(
    entityName: string,
    expectedBody: Partial<TEntity>,
    query?: Partial<TEntity>
  ): string {
    return `{
             ${entityName}${this.formatQuery(query)} {
                ${this.createBody(expectedBody)}
             }
           }
        `;
  }

  private static formatQuery(query?: Record<string, unknown>): string {
    if (query == undefined) {
      return "";
    }
    const keys = Object.keys(query);
    const formattedQuery = keys.map((key) => `${[key]}: "${query[key]}"`);
    const graphqlQuery = formattedQuery.reduce((a, b) => a.concat(b));
    return `(${graphqlQuery})`;
  }

  private static createBody(object: Record<string, unknown>): string {
    const keys = Object.keys(object);
    const objectKeys = keys.map((x) => `${x}`);
    return objectKeys.reduce((a, b) => `${a},\n${b}`);
  }
}
