export enum ApolloRequestType {
  mutation = "mutation",
  query = "query",
}
