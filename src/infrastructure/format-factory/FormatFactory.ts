import moment from "moment";
import {CustomInputFormat} from "../../components/cross-platform/general/CustomInput";

export class FormatFactory {
  public static getDeliveryDateFormat(): CustomInputFormat {
    const currentHour = moment().hours();
    const currentMinute = moment().minutes();
    const hours = Array.from({ length: 24 }, (v, k) => k + 1);
    const minutes = [0, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
    const minuteOptions = minutes.filter((x) => x >= currentMinute);
    const hourOptions = hours.filter((x) => x >= currentHour && x <= 20);
    return [
      { length: [1, 2], options: hourOptions, placeholder: "Hour" },
      { fixed: " : " },
      { length: [1, 2], placeholder: "Minutes", options: minuteOptions },
    ];
  }
}
