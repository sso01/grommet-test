export interface IReview {
  readonly authorName: string;
  readonly profilePicture: string;
  readonly rating: number;
  readonly description: string;
  readonly time: string;
}

export interface IGoogleReview {
  readonly placeId: string;
  readonly name: string;
  readonly address: string;
  readonly rating: number;
  readonly totalReviews: number;
  readonly reviews: IReview[];
}

export interface IGoogleReviewService {
  getInformation(placeId: string): Promise<IGoogleReview>;
}
