import { IGoogleReview } from "./IGoogleReviewService";
import { Client } from "@googlemaps/google-maps-services-js";
import axios from "axios";

export class GoogleReviewService {
  public static async getInformation(placeId: string): Promise<IGoogleReview> {
    // ChIJf5IaSSovmEcRAwirLjBfQE0
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/place/details/json?key=${process.env.GOOGLE_API_KEY}&place_id=${placeId}&language=en`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
    const data = await response.json()
    return {
      address: data.result.formatted_address,
      name: data.result.name,
      placeId,
      rating: data.result.rating,
      totalReviews: data.result.user_ratings_total,
      reviews: data.result.reviews.map((review) => ({
        authorName: review.author_name,
        profilePicture: review.profile_photo_url,
        rating: review.rating,
        description: review.text,
        time: review.relative_time_description,
      })),
    };
  }
}
